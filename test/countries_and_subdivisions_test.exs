defmodule CountriesAndSubdivisionsTest do
  use ExUnit.Case
  doctest CountriesAndSubdivisions

  test "test country" do
    country = CountriesAndSubdivisions.Countries.country("DE")
    assert country["name"] == "Germany"
  end

  test "test subdivision" do
    sub = CountriesAndSubdivisions.Countries.subdivision("DE", "BE")
    assert sub == "Berlin"
  end

  test "test subdivision all" do
    sub = CountriesAndSubdivisions.Countries.subdivision_all("DE")
    assert sub["BE"] == "Berlin"
  end
end
